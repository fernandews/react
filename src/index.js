import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { presetToOptions } from 'webpack/lib/Stats';
import { AST_False } from 'terser';

// tem que ter uma raiz única, não pode ter duas coisas
// const elemento = (
//     <p>Funciona!</p>
// );
// Teria que colocar lá no render pra ir pro body

// Sempre começa em maiúsculo
// function Hello(props) {
//     return <h1>Alô alô, {props.msg}</h1>
// }
// Vc declara isso no render com <Hello msg="blablbla" />

//  No componente baseado em classe tb tem conceito de propriedade, 
// um componente funcional recebe props como argumento, no caso da classe props já é um atributo do próprio objeto
class Clock extends React.Component {
    constructor (props) {
        // tem que chamar o construtor do pai e passar props pra não dar ruim
        super(props);
        this.state = {
            date: new Date(),
            loading: true
        };
    }

    //  pra fazer alterações de estado vc chama métodos ao longo do ciclo de vida do component
    // chamado qnd o componente for ser exibido
    componentDidMount() {
        // aqui ele vai setar um intervalo e chamar o tick a cada 1seg
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
    }

    tick = () => {
        //  não altera o state na mão que não funciona, chama o método
        this.setState({
            date: new Date(),
            loading: false,
        });
        //  se fizer c função fixa o state e se o state ou props ou enfimmudar em algum momento vc garante que não vai ter conflito de dado
        //  mesmo que ocorram alterações, não vai mudar o que acontece na função
        //  bom se vc quiser evitar um erro causado por uma informação que pode ser mudada
    };

    // Vai ser encerrado
    componentWillUnmount() {
        // tira o intervalo p não ficar o timer consummindo o processamento
        clearInterval(this.timerID);
    }

    render() {
        return (
            <div>
                <h1>Alô alô, {this.props.name}</h1>
                <h2>Hora: {this.state.date.toLocaleTimeString()}</h2>
            </div>
        )
    }
}

function App () {
    return (
        <div>
            <Clock name="Paula"/>
            <Clock name="Josefredo"/>
            <Clock name="MUNDOOOOOOOO"/>
        </div>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();